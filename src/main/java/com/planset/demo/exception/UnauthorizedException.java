package com.planset.demo.exception;

public class UnauthorizedException extends Throwable {
    public UnauthorizedException(String message) {
        super(message);
    }
}
